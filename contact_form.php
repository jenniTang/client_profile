<?php
require "connection.php";

	$sql = 'SELECT * FROM tbl_client';

			$stat = $conn->prepare($sql);
			$stat->execute();

			$list = $stat->fetchall(PDO::FETCH_OBJ);

include 'header.php';
?>



	<div>
		<nav class="navbar navar-default">
			<div class="container-fluid">
				<div class="navbar-header">
				</div>
				<center>
				<div class="row" style="margin-top: 50px;">
				<div class="col-md-3"></div>
				<div class="col-md-6">
				<form action="create_client.php" method="get">
					<table id="example" class="display" style="width:100%">
						 <thead>
				            <tr>
				                <th></th>
				                <th></th>
				               
				            </tr>
				        </thead>
				        <tbody class="datashow">
						<?php foreach($list as $data): ?>
							<tr>
								<td>
					       		 	<p><img src="uploads/<?= $data->image; ?>" height="150" width="150" /></p>
					        		<!-- <input class="input-group" type="file" name="image" accept="image/*" /> -->
					       		</td>
					       		<td>
									<label>Client Name: <?= $data->name; ?>
										<br>Contact Number: <?= $data->contact_num; ?>
										<?php
										$comp_id = $data->company_id;
										$sql = 'SELECT * FROM tbl_company WHERE company_id = :comp_id';
										$stat = $conn->prepare($sql);
										$stat->execute([':comp_id' => $comp_id]);

										$data2 = $stat->fetch(PDO::FETCH_OBJ); 
										?>
										<br>Company: <?= $data2->name; ?> | <?= $data2->address; ?>  
									</label>
									<!-- <input type="text" name="username" id="username" placeholder="Username"> -->
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
				        <tfoot>
				            <tr>
				                <th></th>
				                <th></th>
				            </tr>
				        </tfoot>
					</table>
				</form>
				</div>
				<div class="col-md-3"></div>
				</div>
				</center>
			</div>
		</nav>
	</div>
<?php include'footer.php';?>