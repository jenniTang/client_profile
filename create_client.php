<?php

// error_reporting( ~E_NOTICE );
require_once 'connection.php';

		$sql = "SELECT * FROM tbl_company";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data = $stat->fetchall(PDO::FETCH_OBJ);



    if(isset($_POST['submit'])){
try{
	    $name = $_POST['name'];
		$contact_num = $_POST['contact_num'];
		$user_id = 1;
		$imgFile = $_FILES['user_image']['name'];
		$tmp_dir = $_FILES['user_image']['tmp_name'];
		$imgSize = $_FILES['user_image']['size'];
				$company_name =  $_POST['company_name'];
				$sql = "SELECT * FROM tbl_company WHERE name=:company_name";
					$stat = $conn->prepare($sql);
					$stat->execute([':company_name'=> $company_name]);
					$v = $stat->fetch(PDO::FETCH_OBJ);
					$company_id = $v->company_id;

			if(empty($name)){
				$errMSG = "Please Enter Username.";
			}
			else if(empty($contact_num)){
				$errMSG = "Please Enter Your Contact Number.";
			}
			else if(empty($company_name)){
				$errMSG = "Please Select Company.";
			}
			else if(empty($imgFile)){
				$errMSG = "Please Select Image File.";
			}
			else
			{
				$upload_dir = 'uploads/';
				$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
				$valid_extensions = array('jpeg', 'jpg', 'png', 'gif');
				$userprofile = rand(1000,1000000).".".$imgExt;
				if(in_array($imgExt, $valid_extensions)){
					if($imgSize < 5000000)				{
						move_uploaded_file($tmp_dir,$upload_dir.$userprofile);
					}
					else{
						$errMSG = "Sorry, Your File Is Too Large To Upload. It Should Be Less Than 5MB.";
						echo "<br>" . $errMSG;
					}
				}
				else{
					$errMSG = "Sorry, only JPG, JPEG, PNG & GIF Extension Files Are Allowed.";
					echo "<br>" . $errMSG;		
				}
			if(!isset($errMSG))
			{
			$sql = 'INSERT INTO tbl_client(name,contact_num,company_id,user_id,image) VALUES(:name,:contact_num,:company_id,:user_id,:image)';

		    $stat = $conn->prepare($sql);
		    $stat->execute([':name'=> $name, ':contact_num' => $contact_num, ':company_id' => $company_id, ':user_id' => $user_id, ':image' => $userprofile]);
		    
		    echo "New record created successfully";
		    echo "<br>";

		   		header("Location: contact_form.php");
			}
		}
	}
	catch(PDOExeception $e){
		echo "FAILED!".$e->getMessage();
	}
	}

include "header.php"; ?>
	
	<form action="" method="post"  enctype="multipart/form-data" style="margin-top: 100px;">
		<!-- <div class="row">
			<div class="col-md-12"></div>
		</div> -->
		<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-2">
				 	<p><img src="uploads/user.png" height="200" width="150" /></p>
						<input class="input-group" type="file" name="user_image" accept="image/*" />
				</div>
				<div class="col-md-7">
					<label style="margin-left: 75px; font-size: 25px; color: #0000FF"> CREATE CLIENT </label>
					<div>
						<label for="name">Username: </label>
							<input type="text" name="name" id="name" placeholder="Username">
					</div>
					<div>
						<label for="contact_num">Contact Number: </label>
							<input type="text" name="contact_num" id="contact_num" placeholder="Contact Number">
					</div>
					<div>
						<label>Company Name</label>
						<select name="company_name">
							<option>choose company</option>
							<?php foreach($data as $value):?>
							<option><?= $value->name; ?></option>
							<?php endforeach; ?>
						</select>
					<div>
						<input type="submit" name="submit" id="submit" value="Add Client">
					
					</div>
				</div>
		</div>
	</form>

<?php include "footer.php"; ?>